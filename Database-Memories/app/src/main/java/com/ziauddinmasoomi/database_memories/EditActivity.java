package com.ziauddinmasoomi.database_memories;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TimePicker;

import com.ziauddinmasoomi.database_memories.database.MemoryFunctions;
import com.ziauddinmasoomi.database_memories.model.Memory;

import java.util.Calendar;

public class EditActivity extends AppCompatActivity {

    private Intent takenInten,imageIntent;
    private EditText title,location,description,time_text_view,date_text_view;
    private Button save,reset;
    private ImageButton date,time;
    private ImageView imageView;
    private Memory memory;
    private MemoryFunctions memoryFunctions;

    private String amPmStr;
    private static final int IMAGE_CODE = 6;
    private int year,month,day,minute,hour;
    private Calendar calendar;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private int amPm ;
    private long updated;

    private Uri uri;
    private Cursor cursor;
    private int columnIndex;
    private String[] pictures;
    private String titleStr,locationStr,descriptionStr,dateStr,timeStr,filePath;
    private Bitmap selectedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        memoryFunctions = new MemoryFunctions(EditActivity.this);
        imageView = (ImageView) findViewById(R.id.image_viewer_edit);
        title = (EditText) findViewById(R.id.title_edit);
        time_text_view = (EditText) findViewById(R.id.time_text_view_edit);
        date_text_view = (EditText) findViewById(R.id.date_text_view_edit);
        location = (EditText) findViewById(R.id.location_edit);
        description = (EditText) findViewById(R.id.description_edit);
        date = (ImageButton) findViewById(R.id.date_chooser_edit);
        time = (ImageButton) findViewById(R.id.time_chooser_edit);
        save = (Button) findViewById(R.id.save_button_edit);
        reset = (Button) findViewById(R.id.reset_button_edit);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR);
        minute = calendar.get(Calendar.MINUTE);
        amPm = calendar.get(Calendar.AM_PM);

        initialize();
    }

    private void initialize(){

        takenInten = getIntent();
        memory = (Memory) takenInten.getParcelableExtra(getString(R.string.updte_memory));

        title.setText(memory.getTitle());
        time_text_view.setText(memory.getTime());
        date_text_view.setText(memory.getDate());
        location.setText(memory.getLocation());
        description.setText(memory.getDescription());

        selectedImage = BitmapFactory.decodeFile(memory.getImagePath());
        imageView.setImageBitmap(selectedImage);

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog = new DatePickerDialog(EditActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        date_text_view.setText(dayOfMonth+"-"+month+"-"+year);
                    }
                },year,month,day);
                datePickerDialog.show();
            }
        });

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog = new TimePickerDialog(EditActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        amPmStr = ( amPm == Calendar.AM) ? " AM " : " PM ";
                        time_text_view.setText( hourOfDay+":"+minute +" "+amPmStr);
                    }
                },hour,minute,false);
                timePickerDialog.show();
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(imageIntent,IMAGE_CODE);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleStr = title.getText().toString().trim();
                timeStr = time_text_view.getText().toString().trim();
                dateStr = date_text_view.getText().toString().trim();
                locationStr = location.getText().toString().trim();
                descriptionStr = description.getText().toString().trim();

                updated = memoryFunctions.editMemoryById(memory.getId(),titleStr,dateStr,timeStr,
                        locationStr,descriptionStr,filePath);

                if (updated == 1){
                    setResult(MainActivity.UPDATE_MEMORY_CODE_RESULT);
                    finish();
                }

            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title.setText("");
                time_text_view.setText("");
                date_text_view.setText("");
                location.setText("");
                description.setText("");
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case IMAGE_CODE:
                if (resultCode == RESULT_OK){
                    uri = data.getData();

                    Log.d("3333", "onActivityResult: "+ uri);

                    pictures = new String[]{MediaStore.Images.Media.DATA};

                    cursor = getContentResolver().query(uri,pictures,null,null,null);
                    cursor.moveToFirst();

                    columnIndex = cursor.getColumnIndex(pictures[0]);
                    filePath = cursor.getString(columnIndex);
                    cursor.close();

                    selectedImage = BitmapFactory.decodeFile(filePath);
                    imageView.setImageBitmap(selectedImage);
                }
        }
    }

}
