package com.ziauddinmasoomi.database_memories;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ziauddinmasoomi.database_memories.database.MemoryFunctions;
import com.ziauddinmasoomi.database_memories.model.Memory;

import java.util.Calendar;

public class AddActivity extends AppCompatActivity {

    private Button save,reset;
    private ImageButton date,time;
    private ImageView imageView;
    private EditText title,location,description,date_text_view,time_text_view;
    private String titleStr,locationStr,descriptionStr,dateStr,timeStr,filePath,amPmStr;
    private boolean emptyCondition = false;
    private Bitmap selectedImage;
    private Intent memoryIntent,imageIntent;
    private Memory memory;
    private Uri uri;
    private Cursor cursor;
    private int columnIndex;
    private String[] pictures;
    private String imagePath;
    private static final int IMAGE_CODE = 5;
    private int year,month,day,minute,hour;
    private Calendar calendar;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private int amPm ;
    private long added;

    private MemoryFunctions memoryFunctions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        initialization();
    }

    private void initialization() {

        memoryFunctions = new MemoryFunctions(AddActivity.this);
        imageView = (ImageView) findViewById(R.id.image_viewer_add);

        date = (ImageButton) findViewById(R.id.date_chooser_add);
        time = (ImageButton) findViewById(R.id.time_chooser_add);

        save = (Button) findViewById(R.id.save_button_add);
        reset = (Button) findViewById(R.id.reset_button_add);

        title = (EditText) findViewById(R.id.title_add);
        time_text_view = (EditText) findViewById(R.id.time_text_view_add);
        date_text_view = (EditText) findViewById(R.id.date_text_view_add);
        location = (EditText) findViewById(R.id.location_add);
        description = (EditText) findViewById(R.id.description_add);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR);
        minute = calendar.get(Calendar.MINUTE);
        amPm = calendar.get(Calendar.AM_PM);
    }

    @Override
    protected void onStart() {
        super.onStart();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                titleStr = title.getText().toString().trim();
                timeStr = time_text_view.getText().toString().trim();
                dateStr = date_text_view.getText().toString().trim();
                locationStr = location.getText().toString().trim();
                descriptionStr = description.getText().toString().trim();

                emptyCondition = titleStr.equals("") || timeStr.equals("") ||
                        dateStr.equals("") || locationStr.equals("") ||
                        descriptionStr.equals("");

                if (emptyCondition){
                    Toast.makeText(AddActivity.this,"Please fill in all the fields.",Toast.LENGTH_SHORT).show();
                    return;
                }
                else {
                   added =  memoryFunctions.addMemory(titleStr,dateStr,timeStr,locationStr,descriptionStr,imagePath);
                    setResult(MainActivity.ADD_MEMORY_CODE_RESULT);
                    finish();
                }
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                title.setText("");
                time_text_view.setText("");
                date_text_view.setText("");
                location.setText("");
                description.setText("");

            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(imageIntent,IMAGE_CODE);
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog = new DatePickerDialog(AddActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        date_text_view.setText(dayOfMonth+"-"+month+"-"+year);
                    }
                },year,month,day);
                datePickerDialog.show();
            }
        });

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog = new TimePickerDialog(AddActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        amPmStr = ( amPm == Calendar.AM) ? " AM " : " PM ";
                        time_text_view.setText( hourOfDay+":"+minute +" "+amPmStr);
                    }
                },hour,minute,false);
                timePickerDialog.show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case IMAGE_CODE:
                if (resultCode == RESULT_OK){
                    uri = data.getData();

                    pictures = new String[]{MediaStore.Images.Media.DATA};

                    cursor = getContentResolver().query(uri,pictures,null,null,null);
                    cursor.moveToFirst();

                    columnIndex = cursor.getColumnIndex(pictures[0]);
                    imagePath = cursor.getString(columnIndex);
                    cursor.close();

                    selectedImage = BitmapFactory.decodeFile(imagePath);
                    imageView.setImageBitmap(selectedImage);
                }
        }
    }
}
