package com.ziauddinmasoomi.database_memories.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ziauddinmasoomi.database_memories.model.Memory;

import java.util.ArrayList;

/**
 * Created by Zia Uddin Masoomi on 4/23/2017.
 */

public class MemoryFunctions {

    long addedMemory;

    MemoryConfig memoryConfig ;

   public MemoryFunctions(Context context){
       memoryConfig = new MemoryConfig(context);
       memoryConfig.getWritableDatabase();
   }

    public long addMemory(String title,String date,String time,String location,String description,String imagePath){
        SQLiteDatabase database = memoryConfig.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(memoryConfig.COL_TITLE,title);
        contentValues.put(memoryConfig.COL_DATE,date);
        contentValues.put(memoryConfig.COL_TIME,time);
        contentValues.put(memoryConfig.COL_LOCATION,location);
        contentValues.put(memoryConfig.COL_DESCRIPTION,description);
        contentValues.put(memoryConfig.COL_IMAGE_SELECTED,imagePath);

        addedMemory = database.insert(memoryConfig.TBL_NAME,null,contentValues);
        database.close();
        return addedMemory;
    }

    public ArrayList<Memory> selectMemories(){

        ArrayList<Memory> arrayList = new ArrayList<>();
        SQLiteDatabase datebase = memoryConfig.getWritableDatabase();

        String[] columns = {memoryConfig.COL_ID,memoryConfig.COL_TITLE,memoryConfig.COL_TIME,memoryConfig.COL_DATE,
                memoryConfig.COL_LOCATION,memoryConfig.COL_DESCRIPTION,memoryConfig.COL_IMAGE_SELECTED};

        Cursor cursor = datebase.query(memoryConfig.TBL_NAME,columns,null,null,null,null,null);

        while (cursor.moveToNext()){

            arrayList.add(new Memory(
                    cursor.getInt(cursor.getColumnIndex(memoryConfig.COL_ID)),
                    cursor.getString(cursor.getColumnIndex(memoryConfig.COL_IMAGE_SELECTED)),
                    cursor.getString(cursor.getColumnIndex(memoryConfig.COL_TITLE)),
                    cursor.getString(cursor.getColumnIndex(memoryConfig.COL_TIME)),
                    cursor.getString(cursor.getColumnIndex(memoryConfig.COL_DATE)),
                    cursor.getString(cursor.getColumnIndex(memoryConfig.COL_LOCATION)),
                    cursor.getString(cursor.getColumnIndex(memoryConfig.COL_DESCRIPTION))));
        }

        cursor.close();
        datebase.close();

        return arrayList;
    }



    public Object[] selectById(int memoryId){
        int counter = 0;
        Object[] memory = null;
        SQLiteDatabase database = memoryConfig.getWritableDatabase();

        String[] columns = {memoryConfig.COL_ID,memoryConfig.COL_TITLE,memoryConfig.COL_TIME,memoryConfig.COL_DATE,
                memoryConfig.COL_LOCATION,memoryConfig.COL_DESCRIPTION,memoryConfig.COL_IMAGE_SELECTED};
        String[] whereArgs = {memoryId+""};
        String selection = memoryConfig.COL_ID+" = ? ";

        Cursor cursor = database.query(memoryConfig.TBL_NAME,columns,selection,whereArgs,null,null,null);

        while (cursor.moveToNext()){
            memory = new Object[]{
                    cursor.getInt(cursor.getColumnIndex(memoryConfig.COL_ID)),
                    cursor.getString(cursor.getColumnIndex(memoryConfig.COL_IMAGE_SELECTED)),
                    cursor.getString(cursor.getColumnIndex(memoryConfig.COL_TITLE)),
                    cursor.getString(cursor.getColumnIndex(memoryConfig.COL_TIME)),
                    cursor.getString(cursor.getColumnIndex(memoryConfig.COL_DATE)),
                    cursor.getString(cursor.getColumnIndex(memoryConfig.COL_LOCATION)),
                    cursor.getString(cursor.getColumnIndex(memoryConfig.COL_DESCRIPTION))
            };
        }
        cursor.close();
        database.close();

        return memory;
    }


    public long editMemoryById(int id,String title,String date,String time,String location,String description,String imagePath){
        long udpated;
        SQLiteDatabase database  = memoryConfig.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(memoryConfig.COL_TITLE,title);
        contentValues.put(memoryConfig.COL_DATE,date);
        contentValues.put(memoryConfig.COL_TIME,time);
        contentValues.put(memoryConfig.COL_LOCATION,location);
        contentValues.put(memoryConfig.COL_DESCRIPTION,description);
        contentValues.put(memoryConfig.COL_IMAGE_SELECTED,imagePath);

        String[] whereArgs = {id+""};
        String selections = memoryConfig.COL_ID+" = ? ";

        udpated = database.update(memoryConfig.TBL_NAME,contentValues,selections,whereArgs);
        database.close();
        return udpated;
    }


    public long deleteMemoryById(int id){
        long deleted;
        SQLiteDatabase database = memoryConfig.getWritableDatabase();

        String whereClause = memoryConfig.COL_ID+" = ? ";
        String[] whereArgs = {id+" "};

        deleted = database.delete(memoryConfig.TBL_NAME,whereClause,whereArgs);
        return deleted;
    }

}
