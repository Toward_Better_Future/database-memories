package com.ziauddinmasoomi.database_memories.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Zia Uddin Masoomi on 4/23/2017.
 */

public class Memory implements Parcelable{

    private int id ;
    private String imagePath;
    private String title;
    private String time;
    private String date;
    private String location;
    private String description;

    public Memory(int id,String imagePath, String title, String time, String date, String location, String description) {
        this.id = id;
        this.imagePath = imagePath;
        this.title = title;
        this.time = time;
        this.date = date;
        this.location = location;
        this.description = description;
    }

    public Memory(String imagePath, String title, String time, String date, String location, String description) {
        this.imagePath = imagePath;
        this.title = title;
        this.time = time;
        this.date = date;
        this.location = location;
        this.description = description;
    }

    protected Memory(Parcel in) {
        id = in.readInt();
        imagePath = in.readString();
        title = in.readString();
        time = in.readString();
        date = in.readString();
        location = in.readString();
        description = in.readString();
    }

    public static final Creator<Memory> CREATOR = new Creator<Memory>() {
        @Override
        public Memory createFromParcel(Parcel in) {
            return new Memory(in);
        }

        @Override
        public Memory[] newArray(int size) {
            return new Memory[size];
        }
    };


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(imagePath);
        dest.writeString(title);
        dest.writeString(time);
        dest.writeString(date);
        dest.writeString(location);
        dest.writeString(description);
    }

    @Override
    public String toString() {
        return getTitle();
    }
}
