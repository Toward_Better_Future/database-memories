package com.ziauddinmasoomi.database_memories;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;

import com.ziauddinmasoomi.database_memories.model.Memory;

public class DetailsActivity extends AppCompatActivity {

    private Intent takenInten;
    private EditText title,time,date,location,description;
    private ImageView imageView;
    private Bitmap bitmap ;
    private Memory memory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        imageView = (ImageView) findViewById(R.id.image_viewer_details);
        title = (EditText) findViewById(R.id.title_details);
        time = (EditText) findViewById(R.id.time_details);
        date = (EditText) findViewById(R.id.date_details);
        location = (EditText) findViewById(R.id.location_details);
        description = (EditText) findViewById(R.id.description_details);

    }

    @Override
    protected void onStart() {
        super.onStart();

        takenInten = getIntent();
        memory = (Memory) takenInten.getParcelableExtra(getString(R.string.details_memory));

        title.setText("Title : "+memory.getTitle());
        time.setText("Time : "+memory.getTime());
        date.setText("Date : "+memory.getDate());
        location.setText("Location : "+memory.getLocation());
        description.setText("Description : "+memory.getDescription());
        bitmap = BitmapFactory.decodeFile(memory.getImagePath());
        imageView.setImageBitmap(bitmap);

    }
}
