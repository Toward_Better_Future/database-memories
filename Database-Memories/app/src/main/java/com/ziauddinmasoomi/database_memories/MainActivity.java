package com.ziauddinmasoomi.database_memories;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.ziauddinmasoomi.database_memories.database.MemoryConfig;
import com.ziauddinmasoomi.database_memories.database.MemoryFunctions;
import com.ziauddinmasoomi.database_memories.model.Memory;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Intent intent;
    private Memory updateMemory,detailsMemory,takenMemory;
    private ArrayList<Memory> arrayList;
    private ArrayAdapter<Memory> adapter;
    private ListView listView;
    private SearchView searchView;
    private MenuItem menuItem;
    private MenuInflater menuInflater;
    private int selectedItem ;
    private Toolbar toolbar;
    private FloatingActionButton fab;

    private AlertDialog.Builder builder;
    private AlertDialog alert;
    private MemoryConfig memoryConfig;
    private MemoryFunctions memoryFunctions;

    public static final int ADD_MEMORY_CODE_SEND = 1;
    public static final int UPDATE_MEMORY_CODE_SEND = 2;
    public static final int ADD_MEMORY_CODE_RESULT = 3;
    public static final int UPDATE_MEMORY_CODE_RESULT = 4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialization();
    }

    private void initialization() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.list_view_memories);
        registerForContextMenu(listView);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        memoryConfig = new MemoryConfig(MainActivity.this);
        memoryFunctions = new MemoryFunctions(MainActivity.this);
        memoryConfig.getWritableDatabase();
        arrayList = memoryFunctions.selectMemories();

    }

    @Override
    protected void onStart() {
        super.onStart();

        adapter = new ArrayAdapter<Memory>(MainActivity.this,R.layout.row_layout,memoryFunctions.selectMemories());
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                detailsMemory = (Memory)parent.getAdapter().getItem(position);
                intent = new Intent(MainActivity.this,DetailsActivity.class);
                intent.putExtra(getString(R.string.details_memory),detailsMemory);
                startActivity(intent);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                updateMemory = (Memory) parent.getAdapter().getItem(position);
                return false;
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this,AddActivity.class);
                startActivityForResult(intent, ADD_MEMORY_CODE_SEND);
            }
        });

    }

    /** this method is used to filter the list.*/
    public boolean onCreateOptionsMenu(Menu menu) {
        menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menubar,menu);

        menuItem = menu.findItem(R.id.search_view_bar);
        searchView = (SearchView) MenuItemCompat.getActionView(menuItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }

    /** this method is used to recognize the selected list.*/
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.list_view_memories){
            MainActivity.this.getMenuInflater().inflate(R.menu.context_menu,menu);
        }
    }

    /** this method is used to check what the selection from context-menu is.*/
    public boolean onContextItemSelected(MenuItem item) {

        selectedItem = item.getItemId();

        switch (selectedItem){
            case R.id.edit: {
                Log.d("Testing", updateMemory.getId() + " , " + updateMemory.getTitle());
                intent = new Intent(MainActivity.this, EditActivity.class);
                intent.putExtra(getString(R.string.updte_memory), updateMemory);
                startActivityForResult(intent, UPDATE_MEMORY_CODE_SEND);
            }
                break;
            case R.id.delete: {


                builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Are you sure you want to delete ?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        memoryFunctions.deleteMemoryById(updateMemory.getId());
                        Toast.makeText(MainActivity.this, "The memory was deleted successfully.", Toast.LENGTH_SHORT).show();
                        listView.setAdapter(adapter);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.show();
                alert = builder.create();
                alert.setIcon(getResources().getDrawable(R.drawable.ic_delete_forever_black_24dp));
            }
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case ADD_MEMORY_CODE_SEND:
                if (resultCode == ADD_MEMORY_CODE_RESULT){
                    Toast.makeText(MainActivity.this,"The memroy saved successfully.",Toast.LENGTH_LONG).show();
                    listView.setAdapter(adapter);
                }
                break;
            case UPDATE_MEMORY_CODE_SEND:
                if (requestCode == UPDATE_MEMORY_CODE_RESULT){
                    Toast.makeText(MainActivity.this,"The memory updated successfully.",Toast.LENGTH_LONG).show();
                    listView.setAdapter(adapter);
                }
                break;
            default:

        }

    }
}
