package com.ziauddinmasoomi.database_memories.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.content.ContentValues.TAG;

/**
 * Created by Zia Uddin Masoomi on 4/23/2017.
 */

public class MemoryConfig extends SQLiteOpenHelper{

    public static final String DB_NAME = "memories";
    public static final String TBL_NAME = "memory";
    public static final String COL_ID ="id";
    public static final String COL_TITLE = "title";
    public static final String COL_DATE = "date";
    public static final String COL_TIME = "time";
    public static final String COL_LOCATION = "location";
    public static final String COL_DESCRIPTION = "description";
    public static final String COL_IMAGE_SELECTED = "selected_image";
    public static final int VERSION = 3;

    public static final String CREATE_TABLE_MEMORY = "create table if not exists "+TBL_NAME+" ("
            + COL_ID +" integer primary key autoincrement, "
            + COL_TITLE +" varchar(100) not null,"
            + COL_TIME +" varchar(10) not null,"
            + COL_DATE+" varchar(10) not null,"
            + COL_LOCATION+" varchar(50) not null,"
            + COL_DESCRIPTION+" varchar(200) not null,"
            + COL_IMAGE_SELECTED+" varchar(255));";

    public static final String DROP_TABLE_MEMORY = "drop table if exists "+TBL_NAME+" ;";
//    public static final String UPDATE_TABLE_MEMORY ;
//    public static final String SELECT_TABLE_MEMORY ;
//    public static final String INSERT_INTO_TABLE_MEMORY ;


    public MemoryConfig(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        try {
            db.execSQL(CREATE_TABLE_MEMORY);
        }
        catch (SQLiteException e){
            e.getStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL(DROP_TABLE_MEMORY);
            onCreate(db);
        }
        catch (SQLiteException e){
            e.getStackTrace();
        }
    }
}
